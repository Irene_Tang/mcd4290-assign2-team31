// Code for the Measure Run page.
function displayElementsWithClass(className, display)
{
    var elements = document.getElementsByClassName(className);
    for (var i = 0; i < elements.length; i++)
    {
        if (display)
        {
            elements[i].style.display = "block";
        }
        else
        {
            elements[i].style.display = "none";
        }
    }
}
// ======================================================================
//   GPS sensor code (geolocation)
// ======================================================================
if (navigator.geolocation)
{
    positionOptions = {
        enableHighAccuracy: true,
        timeout: Infinity,
        maximumAge: 0
    };
    displayElementsWithClass("gpsError", false);
    navigator.geolocation.watchPosition(showCurrentLocation,errorHandler,positionOptions);
}
else
{
    displayElementsWithClass("gpsValue", false);
}
function errorHandler(error)
{
    if (error.code == 1)
    {
        alert("Location access denied by user.");
    }
    else if (error.code == 2)
    {
        alert("Location unavailable.");
    }
    else if (error.code == 3)
    {
        alert("Location access timed out");
    }
    else
    {
        alert("Unknown error getting location.");
    }
}

//create an empty array to store all loation marker
let markerArray = [];
//store start location marker
let startMarker = [];
//create an empty array to store immediate location marker
let immeMarker = [];
//create an array to store destination location marker
let endMarker = [];
//store all location LngLat after clicking begin run button
let locations = [];
let startLoc = [],endLoc = [];
let map,firstRun;

function showCurrentLocation(position)
{
    // Demonstrate the current latitude and longitude:
    document.getElementById("longValue").innerHTML = Number(position.coords.longitude).toFixed(4);
    Lng1 = Number(position.coords.longitude).toFixed(4);
        
    document.getElementById("latValue").innerHTML = Number(position.coords.latitude).toFixed(4);
    Lat1 = Number(position.coords.latitude).toFixed(4);
    LngLat1 = [Lng1,Lat1];
        
    document.getElementById("accuracyValue").innerHTML = Number(position.coords.accuracy).toFixed(2);
    accuracy = Number(position.coords.accuracy).toFixed(2);
        
    //set start location to marker
    var marker=new mapboxgl.Marker({color: 'red'})
    .setLngLat(LngLat1)
    //store all location markers into the arrray(use push)
    markerArray.push(marker)
	//Set popup to show the information
    var popup1 = new mapboxgl.Popup()
    .setLngLat(LngLat1)
    .setText('Start Location')  
	    
    if(markerArray.length === 1)//set map centre near current location
    {
        map = new mapboxgl.Map({
        container: 'map', // HTML container id
        style: 'mapbox://styles/mapbox/streets-v11',// style URL
        center: LngLat1, // starting position as [lng, lat]
        zoom: 17 // starting zoom  
        });
    }
   //check if it is a repeat run
   if (localStorage.getItem("RepeatRun") !== null)
	{
		disableButton();//disable random destionation button
		/*parse repeat run from local storage and set start and end location*/
		repeatRun = JSON.parse(localStorage.getItem("RepeatRun"));
		repeatStart = [repeatRun._startLoc[0][0],repeatRun._startLoc[0][1]];
		startLoc.push(repeatStart);
		repeatDest = [repeatRun._endLoc[0][0],repeatRun._endLoc[0][1]];
		endLoc.push(repeatDest);
		//calculate start and end distance
		distance1 = calcDistance(repeatStart[0],repeatStart[1],repeatDest[0],repeatDest[1]).toFixed(2);
		//set repeat destination location to maker
        marker= new mapboxgl.Marker({color: 'green'})
        .setLngLat(repeatDest)
	    popup3 = new mapboxgl.Popup()
        .setLngLat(repeatDest)
        .setText('Destination Distance (repeat): '+ distance1+'m')
        popup3.addTo(map);
		//push repeat destination makrer to endMarker
		endMarker.push(marker);
        endMarker[0].addTo(map).setPopup(popup3);
		
		//set repeat start location to marker
		marker=new mapboxgl.Marker({color: 'red'})
		.setLngLat(repeatStart)
        popup4 = new mapboxgl.Popup()
        .setLngLat(repeatStart)
        .setText('Start Location (repeat)')
	    //add start location popup to map
        popup4.addTo(map);
		startMarker.push(marker);
        startMarker[0].addTo(map).setPopup(popup4);
		
		//calculate distance between your current loc and repeat start loc
		repeatDis = calcDistance(Lng1,Lat1,repeatStart[0],repeatStart[1]);
		while(repeatDis > 10)
		{
			displayMessage("You are not close to the start location! "+repeatDis+"m far from start location.")
		}
		if (repeatDis <10)
		{
			displayMessage("You are able to bengin the repeat run!");
			enableButton2();//enable begin run button
		}
	}
	
	//before select the destination, the start location can keep changing
    if(endMarker.length === 0)
    {
        //add start location popup to map and push start location LngLat to startLoc array
        popup1.addTo(map);
        startLoc.push(LngLat1);
		map.panTo(startLoc[0]);//pan to start loc before select dest loc
		m = markerArray.length;
        startMarker.push(markerArray[m-1])//push current marker to start marker
		//before selesct destination,start loc can change
        if (startLoc.length>1)
        {
			startLoc.shift();//remove old startLoc value
			oldStart = startMarker.shift();//remove old marker
            oldStart.remove();//remove from map
        } 
        //Extract from Array amd add start location marker
        startMarker[0].addTo(map).setPopup(popup1);
    }
   
    //Enable the Random destination button when the accuracy value less than 20
    //user hasn't choose destiantion location
    if(accuracy < 20 && localStorage.getItem("RepeatRun") === null)
    {
        enableButton();
    }
    else
    {
        disableButton();
    }
	
    //After enable button, click button will generate destination loc
    document.getElementById("button1").onclick = function()
    {
        /*generate the d(distance) between start location and the first random destination, then check if d meet the requirement (60<d<150)*/
        randomDestination(startLoc[0]);//generate random location base on start location
        /*when d doesn't meet the requirement, it will continue to generate random destination and check again*/
        while(d<60)
        {
            randomDestination(startLoc[0]);
        }
        //distance between start and end locations(used in complete run)
        distance1 = d;
        string = 'Destination Distance:'+d.toFixed(2)+'m';
        //Set popup to show the distance between start and end destination
        var popup2 = new mapboxgl.Popup()
        .setLngLat(LngLat2)
        .setText(string)
        //set marker2 at the random destination location
        var marker2= new mapboxgl.Marker({color: 'green'})
        .setLngLat(LngLat2)
        //store end location marker into an arrray(use push)
        endMarker.push(marker2);
            
        /*when endarray length equal to 1, it already has one end location, so now we want to add new one and remove old one*/
        if(endMarker.length >1)
        {
            //remove the old end location marker
            let removeMarker = endMarker.shift()
            removeMarker.remove()
            //add popup to map
            popup2.addTo(map);
            //Add new destination marker to map and also set popup to show distance
            endMarker[0].addTo(map).setPopup(popup2);
            //remove previous destination LngLat from array(the first element in array)
            endLoc.shift();
            //push new destination LngLat2 to endLoc array;
            endLoc.push(LngLat2);   
        }
        else //just add the first random destination(endArray is empty)
        {
            popup2.addTo(map);
            //Add new destination marker to map and also set popup to show distance
            endMarker[0].addTo(map).setPopup(popup2);
            endLoc.push(LngLat2);
		}
        //after destination generated, begin run button would be able to be clicked
        enableButton2();
    }
    
    //function will run when click begin run button
    document.getElementById("button2").onclick = function()
    { 
		enableButton4();//enable to stop the run
        disableButton2();//disable begin run button
        disableButton();//disable random destination button
        
		/* if it is a repeat run, after begin the run, remove repeat run from local storage after we begin the run*/
		if (localStorage.getItem("RepeatRun") !== null)
		{
			localStorage.removeItem("RepeatRun")
		}
		
        firstRun = new Run();
        Time1 = new Date();//record start time
        date =Time1.getFullYear()+"/"+Time1.getMonth()+"/"+Time1.getDate();
        firstRun.setStartLoc(startLoc);
        firstRun.setEndLoc(endLoc);
        firstRun.setTimeStart(Time1);
        firstRun.setRunDate(date);
		
		//add path line
		pathCoordinates = [startLoc[0],endLoc[0]];
		pathLayer(pathCoordinates);//call back function to add line
		
        //fucntion will repeat every second
        var track = setInterval(function(){
            //push immediate LngLat to locations array
            locations.push(LngLat1);
            j = locations.length;
	
			//set location to marker
			var marker=new mapboxgl.Marker({color: 'red'})
            .setLngLat(LngLat1)
            //add all immediate location marker to immeArray
            immeMarker.push(marker);
			
            //set locations into firstRun object
            firstRun.setLocations(locations)
			//calculate distance bewtween current loc and start loc
			distance2 = firstRun.getDistance();
            //Set popup to show the immediate information
            var popup3 = new mapboxgl.Popup()
            .setLngLat(locations[j-1])
            .setText('Distance from start:'+ distance2 +'m')
			
            i = immeMarker.length;
            if (i > 1)
            {
                //remove previous immediate location marker
                immeMarker[i-2].remove(); 
				//add start location popup to map
				popup3.addTo(map);
                //Add new immediate location marker to map
                immeMarker[i-1].addTo(map).setPopup(popup3);	
            }
            if (i === 1)//just click begin run button
            {
                startMarker[0].remove();//remove start location marker
				//add current location popup to map
				popup3.addTo(map);
                //Add immediate location marker to map
                immeMarker[0].addTo(map).setPopup(popup3); 
			}
			
			pathCoordinates = [locations[j-1],endLoc[0]];
			pathLayer(pathCoordinates);
			//calculate distance between current and end location
			Lng1 = locations[j-1][0];
			Lat1 = locations[j-1][1];
			Lng2 = endLoc[0][0];
			Lat2 = endLoc[0][1];
			distance3 = calcDistance(Lng1,Lat1,Lng2,Lat2).toFixed(2)
			
            //when it is close to destination(difference less than 10m)
            if (distance3 <= 10)//complete
            {
                clearInterval(track);
                Time2 = new Date();//record end time
                firstRun.setTimeEnd(Time2);
				//distance bewtween current loc and start loc
				firstRun.setDistance(distance2);
                enableButton3();//enable save run button
				removeLayerWithId('route')//after complete run, remove line layer
     
                timeDuration = firstRun.getTotalTime();
				//between current and start loc
				distance2 = firstRun.getDistance();
                string1 = "Time taken for the run:"+timeDuration/1000+" s";
                string2 = "Distance travelled: "+distance2+" m";
                alert("Complete run!"+"\n"+string1+"\n"+string2);
				
				//clear markers on map after complete a run
				j = immeMarker.length;
				immeMarker[j-1].remove();
				h = endMarker.length;
				endMarker[0].remove();
				alert("You can save the run or start a new run.")
		
				//create an empty array to store all loation marker
				markerArray = [];
				//create an empty array to store immediate location marker
				immeMarker = [];
				//create an array to store destination location marker
				endMarker = [],locations = [];
				startLoc = [],endLoc = []; 
            }
        },1000)
    }
}

document.getElementById("button4").onclick=function()
{
    location.reload(); //This will reload the page
	alert("The run has been stopped, you can start a new run now!");
}

function saveRun()
{
    name = prompt("Please enter the name of run","Run");
    firstRun.setRunName(name);
    
	//if the 'Run' local storage is not empty, parse all old from local storage and then push new run class into it
    if (localStorage.getItem("Run") !== null)
    {
        savedRuns = JSON.parse(localStorage.getItem("Run"));
        savedRuns.push(firstRun);   
    }
    else
    {
        savedRuns.push(firstRun);
    }
        
    localStorage.setItem("Run",JSON.stringify(savedRuns));	
    location.href = 'index.html'//jump to run list page     
}

function toRadians(degrees)
{
    return degrees * Math.PI / 180;
}
//function that generate random destination base on the start location
function randomDestination(LngLat1)
{
    Lng2 = (Math.random() * (0.002) + (Lng1-0.001)).toFixed(4);
    Lat2 = (Math.random() * (0.002) + (Lat1-0.001)).toFixed(4);
    LngLat2 = [Lng2,Lat2];
    //call back function to calculate d
    calcDistance(Lng1,Lat1,Lng2,Lat2)
}
//Enable and disable function for random destination button
function disableButton()
{
    document.getElementById("button1").disabled = true;            
}
function enableButton()
{
    document.getElementById("button1").disabled = false;
}
//Enable 'Begin Run' button
function enableButton2()
{
    document.getElementById("button2").disabled = false;
}
function disableButton2()
{
    document.getElementById("button2").disabled = true;
}
//save run button
function enableButton3()
{
    document.getElementById("button3").disabled = false;
}
function disableButton3()
{
    document.getElementById("button3").disabled = true;
}
//stop run button
function enableButton4()
{
    document.getElementById("button4").disabled = false;
}
function disableButton4()
{
    document.getElementById("button4").disabled = true;
}
//function to calculate distance
function calcDistance(Lng1,Lat1,Lng2,Lat2)
{
    R = 6371e3; // metres
    rad1 = toRadians(Lat1);
    rad2 = toRadians(Lat2);
    deltLat = toRadians(Lat2-Lat1);
    deltLng = toRadians(Lng2-Lng1);
        
    a = Math.sin(deltLat/2) * Math.sin(deltLat/2) + Math.cos(rad1) * Math.cos(rad2) * Math.sin(deltLng/2) * Math.sin(deltLng/2);
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    return d = R * c;
}
function pathLayer(pathCoordinates)
{
	removeLayerWithId('route');//remove previous layer
	map.addSource('route', {
		'type': 'geojson',
		'data': {
			'type': 'Feature',
			'properties': {},
            'geometry': {
				'type': 'LineString',
				'coordinates': pathCoordinates//all coordinates
			}
		}
	});
	map.addLayer({
		'id': 'route',
		'type': 'line',
		'source': 'route',
		'layout': {
			'line-join': 'round',
			'line-cap': 'round'
		},
		'paint': {
			'line-color': '#888',
			'line-width': 8
		}
	});
}
// This function checks whether there is a map layer with id matching 
// idToRemove.  If there is, it is removed.
function removeLayerWithId(idToRemove)
{
	let hasPoly = map.getLayer(idToRemove)
     if (hasPoly !== undefined)
	 {
		 map.removeLayer(idToRemove)
		 map.removeSource(idToRemove)
	 }
}