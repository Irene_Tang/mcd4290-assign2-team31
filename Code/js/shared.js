// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";
// Array of saved Run objects.
var savedRuns = [];
class Run
{
 constructor(startLoc,endLoc,locations,timeStart,timeEnd,totalTime,distance,runName,runDate)
    {
        this._startLoc = startLoc;//a LngLat object
        this._endLoc = endLoc;//a LngLat object
        this._locations = locations;//an array of LngLat objects
        this._timeStart = timeStart;
        this._timeEnd = timeEnd;
		this._totalTime = totalTime;
		this._distance = distance;//travelled distance
		this._runName = runName;
		this._runDate = runDate;
    }
    getStartLoc(){
        return this._startLoc;
    }
    setStartLoc(newStartLoc){
        this._startLoc = newStartLoc ;
    }
    getEndLoc(){
        return this._endLoc;
    }
    setEndLoc(newEndLoc){
        this._endLoc = newEndLoc;
    }
    getLocations(){
        return this._locations;
    }
    setLocations(newLocations){
        this._locations = newLocations;
    }
    getTimeStart(){
        return this._timeStart;
    }
    setTimeStart(newTimeStart){
        this._timeStart = newTimeStart;
    }
    getTimeEnd(){
        return this._timeEnd;
    }
    setTimeEnd(newTimeEnd){
        this._timeEnd = newTimeEnd;
    }
	getRunName(){
		return this._runName;
	}
    setRunName(newRunName){
        this._runName = newRunName;
    }
	getRunDate()
	{
		return this._runDate;
	}
    setRunDate(newRunDate){
        this._date = newRunDate;
    }
	setDistance(newDistance){
		this._distance = newDistance;
	}
    getDistance(){
		j = this._locations.length;
		R = 6371e3; // metres
		
        rad1 = toRadians(this._startLoc[0][1]);//Lat1
        rad2 = toRadians(this._locations[j-1][1]);//Lat2
        deltLat = toRadians(this._locations[j-1][1]-this._startLoc[0][1]);//Lat2-Lat1
        deltLng = toRadians(this._locations[j-1][0]-this._startLoc[0][0]);//Lng2-Lng1
		
		a = Math.sin(deltLat/2) * Math.sin(deltLat/2) + Math.cos(rad1) * Math.cos(rad2) * Math.sin(deltLng/2) * Math.sin(deltLng/2);
        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        d = R * c;
        this._distance = d.toFixed(2);
        return this._distance;
	}
    getTotalTime(){
        this._totalTime = this._timeEnd.getTime() - this._timeStart.getTime();
        return this._totalTime;
    }  
}
