// Code for the main app page (Past Runs list).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.
function viewRun(runIndex)
{
    //Save the desired run to local storage so it can be accessed from View Run page.
	localStorage.setItem("selectedRun",runIndex);
    //load the View Run page.
    location.href = 'viewRun.html';
}
runs = JSON.parse(localStorage.getItem("Run"));
nameArray = [];
if (runs !== null)
{
	for(i = 0; i < runs.length; i++)
	{
		nameArray.push(runs[i]._runName);
	}
	// List view section heading: Runs list
	let listHTML = "";
	//iterate over the 'runs' array and create HTML list items for each route
	for (var i = 0; i<runs.length; i++)
	{  
		listHTML += "<li class=\"mdl-list__item mdl-list__item--two-line\" onclick=\"viewRun("+i+")\">";//i represents runIndex
		listHTML += "<span class=\"mdl-list__item-primary-content\">";
		listHTML += "<span>" +nameArray[i]+"</span>";
        listHTML += "<span class=\"mdl-list__item-sub-title\">" +"Date:"+runs[i]._date+", Time:"+runs[i]._totalTime/1000+"s, Distance:"+runs[i]._distance+"m"+", Speed:"+(runs[i]._distance/(runs[i]._totalTime/1000)).toFixed(2)+"m/s"+"</span>";
        listHTML += "</span>";
        listHTML += "</li>"
	}
	// Insert the list view elements into the flights list.
	document.getElementById("runs-list").innerHTML = listHTML;
}