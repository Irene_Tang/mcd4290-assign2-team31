// Code for the View Run page.
let runNames = [];
var runIndex = localStorage.getItem("selectedRun");
if (runIndex !== null)
{
	var runs = JSON.parse(localStorage.getItem("Run"));

	document.getElementById("date").innerHTML = runs[runIndex]._date;
	document.getElementById("time").innerHTML = runs[runIndex]._totalTime/1000;
	document.getElementById("distance").innerHTML = runs[runIndex]._distance;
	document.getElementById("speed").innerHTML = (runs[runIndex]._distance/(runs[runIndex]._totalTime/1000)).toFixed(2);
	
    for(i = 0; i < runs.length; i++)
    {
        runNames.push(runs[i]._runName);
	}
	//show the corresponding name 
    document.getElementById("headerBarTitle").textContent = runNames[runIndex];
	
	//start and destination LngLat values
	start = [runs[runIndex]._startLoc[0][0],runs[runIndex]._startLoc[0][1]];
	dest = [runs[runIndex]._endLoc[0][0],runs[runIndex]._endLoc[0][1]];
	//centre map on the selecte run start loc
	map = new mapboxgl.Map({
	container: 'map', // HTML container id
    style: 'mapbox://styles/mapbox/streets-v11',// style URL
    center: start, // starting position as [lng, lat]
    zoom: 17 // starting zoom  
	});
	
	//set start location to marker
    startMarker=new mapboxgl.Marker({color: 'red'})
    .setLngLat(start)
    popup1 = new mapboxgl.Popup()
    .setLngLat(start)
    .setText('Start Location')
	//add start location popup to map
    popup1.addTo(map);
    startMarker.addTo(map).setPopup(popup1);
	
    //set destination location to maker
    destMarker= new mapboxgl.Marker({color: 'green'})
    .setLngLat(dest)
	popup2 = new mapboxgl.Popup()
    .setLngLat(dest)
    .setText('Destination Location')
	//add destination location popup to map
    popup2.addTo(map);
    destMarker.addTo(map).setPopup(popup2);
	
	let pathCoordinates = [];
	pathCoordinates.push(start);
	//push selected run's immediate locations to path coordinates
	for(i = 0; i < runs[runIndex]._locations.length; i++)
	{
		//immediate lnglat value array
		imme = [runs[runIndex]._locations[i][0],runs[runIndex]._locations[i][1]];
		pathCoordinates.push(imme);
	}
	pathCoordinates.push(dest);

	//add path line
	map.on('load', function(){map.addSource('route', {
		'type': 'geojson',
		'data': {
			'type': 'Feature',
			'properties': {},
            'geometry': {
				'type': 'LineString',
				'coordinates': pathCoordinates
			}
		}
	});
	map.addLayer({
		'id': 'route',
		'type': 'line',
		'source': 'route',
		'layout': {
			'line-join': 'round',
			'line-cap': 'round'
		},
		'paint': {
			'line-color': '#888',
			'line-width': 8
		}
	});
							 });
	
	function deleteRun()
	{
		//delete the corresponding index run object 
		runs.splice(runIndex,1);
		//set new local storage
		localStorage.setItem("Run",JSON.stringify(runs));
		//return the user to Runs List (index) page
		location.href = 'index.html'
	}
	function reattempt()
	{
		location.href = 'newRun.html';
		localStorage.setItem("RepeatRun",JSON.stringify(runs[runIndex]));
	}
}


